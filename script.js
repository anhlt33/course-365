"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
};

const gREQUEST_STATUS_OK = 200;
const gREQUEST_CREATE_OK = 201; // status 201 là tạo mới thành công
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm load trang khi f5
function onPageLoading() {
    //load popuplar courses
    getPopularAndTrendingCourses();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm gọi api để lấy all popular courses
function getPopularAndTrendingCourses() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: "GET",
        success: function (paramCourses) {
            //ghi response ra console
            console.log(paramCourses);
            loadDataToPopularArea(paramCourses);//load data popular courses to web
            loadDataToTrendingArea(paramCourses);//load data trending courses to web
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

//hàm load data to popular courses area
function loadDataToPopularArea(paramPopular) {
    var vCount = 0;
    for (var bI = 0; bI < paramPopular.length; bI++) {
        if (paramPopular[bI].isPopular == true) {
            if (vCount == 4) break;
            var bPopularAreaDiv = $("#div-popular-area");
            var bNewDiv = `
                <div class="col-sm-3">
                    <div class="card">
                        <img class="card-img-top" src="${paramPopular[bI].coverImage}">
                        <div class="card-body">
                            <p class="font-weight-bold"><a href="#">${paramPopular[bI].courseName}</a></p>
                            <p><i class="far fa-clock"></i>&nbsp; ${paramPopular[bI].duration} <span>${paramPopular[bI].level}</span></p>
                            <p class="font-weight-bold">${paramPopular[bI].discountPrice}$&nbsp;<del class="font-weight-normal text-secondary">${paramPopular[bI].price}$</del></p>
                        </div>
                        <div class="card-footer">
                            <img class="float-left rounded-circle img-width" src="${paramPopular[bI].teacherPhoto}">&nbsp; ${paramPopular[bI].teacherName} <i class="far fa-bookmark"></i>
                        </div>
                    </div>
                </div>
            `;
            bPopularAreaDiv.append(bNewDiv);
            vCount++;
        }
    }
}

//hàm load data to popular courses area
function loadDataToTrendingArea(paramTrending) {
    var vCount = 0;
    for (var bI = 0; bI < paramTrending.length; bI++) {
        if (paramTrending[bI].isTrending == true) {
            if (vCount == 4) break;
            var bPopularAreaDiv = $("#div-trending-area");
            var bNewDiv = `
                <div class="col-sm-3">
                    <div class="card">
                        <img class="card-img-top" src="${paramTrending[bI].coverImage}">
                        <div class="card-body">
                            <p class="font-weight-bold"><a href="#">${paramTrending[bI].courseName}</a></p>
                            <p><i class="far fa-clock"></i>&nbsp; ${paramTrending[bI].duration} <span>${paramTrending[bI].level}</span></p>
                            <p class="font-weight-bold">${paramTrending[bI].discountPrice}$&nbsp;<del class="font-weight-normal text-secondary">${paramTrending[bI].price}$</del></p>
                        </div>
                        <div class="card-footer">
                            <img class="float-left rounded-circle img-width" src="${paramTrending[bI].teacherPhoto}">&nbsp; ${paramTrending[bI].teacherName} <i class="far fa-bookmark"></i>
                        </div>
                    </div>
                </div>
            `;
            bPopularAreaDiv.append(bNewDiv);
            vCount++;
        }
    }
}
