"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
// biến toàn cục để lưu trữ course id đang dc update or delete. Mặc định = 0
var gCourseId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gCOURSE_COLS = ["id", "courseCode", "courseName", "level", "duration", "teacherName", "isPopular", "isTrending", "price", "discountPrice", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gCOURSE_CODE_COL = 1;
const gCOURSE_NAME_COL = 2;
const gLEVEL_COL = 3;
const gDURATION_COL = 4;
const gTEACHER_NAME_COL = 5;
const gIS_POPULAR_COL = 6;
const gIS_TRENDING_COL = 7;
const gPRICE_COL = 8;
const gDISCOUNT_PRICE_COL = 9;
const gACTION_COL = 10;
//khai báo DataTable & mapping columns
var gCourseTable = $("#course-table").DataTable({
    columns: [
        { data: gCOURSE_COLS[gID_COL] },
        { data: gCOURSE_COLS[gCOURSE_CODE_COL] },
        { data: gCOURSE_COLS[gCOURSE_NAME_COL] },
        { data: gCOURSE_COLS[gLEVEL_COL] },
        { data: gCOURSE_COLS[gDURATION_COL] },
        { data: gCOURSE_COLS[gTEACHER_NAME_COL] },
        { data: gCOURSE_COLS[gIS_POPULAR_COL] },
        { data: gCOURSE_COLS[gIS_TRENDING_COL] },
        { data: gCOURSE_COLS[gPRICE_COL] },
        { data: gCOURSE_COLS[gDISCOUNT_PRICE_COL] },
        { data: gCOURSE_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
                <img class="edit-course" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                <img class="delete-course" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
            `
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới course
$("#btn-add-new-course").on("click", function () {
    onBtnAddNewCourseClick();
});
//gán sự kiện cho nút Add Course(trên modal)
$("#btn-create-course").on("click", function () {
    onBtnCreateCourseClick();
});
//3 - U: gán sự kiện update-sửa 1 course
$("#course-table").on("click", ".edit-course", function () {
    onBtnEditCourseClick(this);
});
//gán sự kiện cho nút Update course(trên modal)
$("#btn-update-course").on("click", function () {
    onBtnUpdateCourseClick();
});
//4 - D: gán sự kiện delete-xóa 1 course
$("#course-table").on("click", ".delete-course", function () {
    onBtnDeleteCourseClick(this);
});
//gán sự kiện cho nút Delete course(trên modal)
$("#btn-confirm-delete-course").on("click", function () {
    onBtnDeleteCourseConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load course to table
    getAllCourses();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewCourseClick() {
    //hiển thị modal trắng lên
    $("#modal-add-course").modal("show");
}
//hàm xử lý sự kiện nút Create user(trên modal)
function onBtnCreateCourseClick() {
    //khai báo obj chứa user data
    var vRequestDataObj = {
        courseCode: "",
        courseName: "",
        coverImage: "",
        level: "",
        duration: "",
        teacherName: "",
        teacherPhoto: "",
        price: "",
        discountPrice: "",
        isPopular: false,
        isTrending: false
    }
    //b1: get data
    getCreateCourseData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateCourseData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validateCourseData
    console.log(vIsValid);
    if (vIsValid) {
        //b3: create course
        $.ajax({
            url: gBASE_URL + "/courses",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleCreateCourseSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}
//hàm xử lý sự kiện Update course từ table
function onBtnEditCourseClick(paramEdit) {
    //lưu thông tin CourseId đang dc edit vào biến toàn cục
    gCourseId = getCourseIdFromButton(paramEdit);
    console.log("Id của course tương ứng = " + gCourseId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getCourseDataByCourseId(gCourseId);
}
//hàm xử lý sự kiện nút Update course(trên modal)
function onBtnUpdateCourseClick() {
    //khai báo obj chứa user data
    var vRequestDataObj = {
        courseCode: "",
        courseName: "",
        coverImage: "",
        level: "",
        duration: "",
        teacherName: "",
        teacherPhoto: "",
        price: "",
        discountPrice: "",
        isPopular: false,
        isTrending: false
    }
    //b1: get data
    getUpdateCourseData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateCourseData(vRequestDataObj);
    if (vIsValid) {
        //b3: update course
        $.ajax({
            url: gBASE_URL + "/courses/" +gCourseId,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateCourseSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}
//hàm xử lý sự kiện Delete course từ table
function onBtnDeleteCourseClick(paramDelete) {
    //lưu thông tin userId đang dc xóa vào biến toàn cục
    gCourseId = getCourseIdFromButton(paramDelete);
    console.log("Id của course tương ứng = " + gCourseId);
    $("#modal-delete-course").modal("show");
}
// hàm xử lý sự kiện delete course modal click
function onBtnDeleteCourseConfirmClick() {
    // B1: Thu thập dữ liệu(k có)
    // B2: Validate delete(k có)
    // B3: call api to delete course
    $.ajax({
        url: gBASE_URL + "/courses/" +gCourseId,
        type: "DELETE",
        contentType: "application/json;charset=UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteCourseSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm gọi api để lấy all list user đăng ký
function getAllCourses() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: "GET",
        success: function (paramCourses) {
            //ghi response ra console
            console.log(paramCourses);
            loadDataToTable(paramCourses);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
// load Courses to DataTable
// in: Courses array
// out: Course table has data
function loadDataToTable(paramCourses) {
    gCourseTable.clear();
    gCourseTable.rows.add(paramCourses);
    gCourseTable.draw();
}
//hàm thu thập create data
function getCreateCourseData(paramCourseObj) {
    paramCourseObj.courseCode = $("#inp-create-coursecode").val().trim();
    paramCourseObj.courseName = $("#inp-create-coursename").val().trim();
    paramCourseObj.coverImage = $("#inp-create-coverimage").val().trim();
    paramCourseObj.level = $("#select-create-level").val();
    paramCourseObj.duration = $("#inp-create-duration").val().trim();
    paramCourseObj.teacherName = $("#inp-create-teachername").val().trim();
    paramCourseObj.teacherPhoto = $("#inp-create-teacherphoto").val().trim();
    paramCourseObj.isPopular = $("#cb-create-popular").prop('checked');
    paramCourseObj.isTrending = $("#cb-create-trending").prop('checked');
    paramCourseObj.price = $("#inp-create-price").val().trim();
    paramCourseObj.discountPrice = $("#inp-create-discountprice").val().trim();
}
//hàm validate data
function validateCourseData(paramCourseObj) {
    if (paramCourseObj.courseCode === "") {
        alert("Course Code is not blank!");
        return false;
    }
    if (paramCourseObj.courseCode.length < 10) {
        alert('Course Code must at least 10 characters.');
        return false;
    }
    if (paramCourseObj.courseName === "") {
        alert("Course Name is not blank!");
        return false;
    }
    if (paramCourseObj.courseName.length < 20) {
        alert('Course Name must at least 20 characters.');
        return false;
    }
    if (paramCourseObj.coverImage === "") {
        alert("Cover Image is not blank!");
        return false;
    }
    if (paramCourseObj.level === "level") {
        alert("Have not choose a level yet!");
        return false;
    }
    if (paramCourseObj.duration === "") {
        alert("Duration is not blank!");
        return false;
    }
    if (paramCourseObj.teacherName === "") {
        alert("Teacher Name is not blank!");
        return false;
    }
    if (paramCourseObj.teacherPhoto === "") {
        alert("Teacher Photo is not blank!");
        return false;
    }
    if (paramCourseObj.price === "") {
        alert("Price is not blank!");
        return false;
    }
    if (isNaN(paramCourseObj.price) || parseInt(paramCourseObj.price) <= 0) {
        alert("Price must be a number and higher than 0!");
        return false;
    }
    if (isNaN(paramCourseObj.discountPrice)) {
        alert("Discount Price must be a number!");
        return false;
    }
    if (parseInt(paramCourseObj.discountPrice) > parseInt(paramCourseObj.price)) {
        alert("Discount Price must not be higher than price!");
        return false;
    }
    return true;
}
//hàm xử lý front-end khi add course thành công
function handleCreateCourseSuccess() {
    alert("Add new course successfully!");
    getAllCourses(); //load lại table
    resetCreateUserForm(); //reset trắng lại form modal add course
    $("#modal-add-course").modal("hide");
}
//hàm xóa trắng form create course
function resetCreateUserForm() {
    $("#inp-create-coursecode").val("");
    $("#inp-create-coursename").val("");
    $("#inp-create-coverimage").val("");
    $("#select-create-level").val("Level");
    $("#inp-create-duration").val("");
    $("#inp-create-teachername").val("");
    $("#inp-create-teacherphoto").val("");
    $("#cb-create-popular").prop('checked', false);
    $("#cb-create-trending").prop('checked', false);
    $("#inp-create-price").val("");
    $("#inp-create-discountprice").val("");
}
//hàm dựa vào button detail (edit or delete) xác định dc id course
function getCourseIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vCourseRowData = gCourseTable.row(vTableRow).data();
    return vCourseRowData.id;
}
//hàm load course data lên update modal form
function getCourseDataByCourseId(paramCourseId) {
    $.ajax({
        url: gBASE_URL + "/courses/" + paramCourseId,
        type: "GET",
        success: function (paramCourseId) {
            showCourseDataToUpdateModal(paramCourseId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#modal-update-course").modal("show");
}
//hàm show course data lên update modal form
function showCourseDataToUpdateModal(paramCourseDataObj) {
    $("#inp-edit-coursecode").val(paramCourseDataObj.courseCode);
    $("#inp-edit-coursename").val(paramCourseDataObj.courseName);
    $("#inp-edit-coverimage").val(paramCourseDataObj.coverImage);
    $("#select-edit-level").val(paramCourseDataObj.level);
    $("#inp-edit-duration").val(paramCourseDataObj.duration);
    $("#inp-edit-teachername").val(paramCourseDataObj.teacherName);
    $("#inp-edit-teacherphoto").val(paramCourseDataObj.teacherPhoto);
    $("#inp-edit-price").val(paramCourseDataObj.price);
    $("#inp-edit-discountprice").val(paramCourseDataObj.discountPrice);
    $("#cb-edit-popular").prop('checked', paramCourseDataObj.isPopular);
    $("#cb-edit-trending").prop('checked', paramCourseDataObj.isTrending);
}
//hàm thu thập update data
function getUpdateCourseData(paramCourseObj) {
    paramCourseObj.courseCode = $("#inp-edit-coursecode").val().trim();
    paramCourseObj.courseName = $("#inp-edit-coursename").val().trim();
    paramCourseObj.coverImage = $("#inp-edit-coverimage").val().trim();
    paramCourseObj.level = $("#select-edit-level").val();
    paramCourseObj.duration = $("#inp-edit-duration").val().trim();
    paramCourseObj.teacherName = $("#inp-edit-teachername").val().trim();
    paramCourseObj.teacherPhoto = $("#inp-edit-teacherphoto").val().trim();
    paramCourseObj.isPopular = $("#cb-edit-popular").prop('checked');
    paramCourseObj.isTrending = $("#cb-edit-trending").prop('checked');
    paramCourseObj.price = $("#inp-edit-price").val().trim();
    paramCourseObj.discountPrice = $("#inp-edit-discountprice").val().trim();
}
//hàm xử lý front-end khi update user thành công
function handleUpdateCourseSuccess() {
    alert("Update course successfully!");
    getAllCourses();
    $("#modal-update-course").modal("hide");
}
//hàm xử lý front-end khi delete course thành công
function handleDeleteCourseSuccess(){
    alert("Delete course successfully!");
    getAllCourses();
    $("#modal-delete-course").modal("hide");
}